import React, { Component } from 'react'
import { connect } from 'react-redux'

import './index.css'

class Details extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userDetails: props.userDetails,
      errors: {
        lastName: '',
        firstName: '',
        age: '',
        mail: '',
        mobile: ''
      }
    }
  }
  componentWillReceiveProps(newProps) {
    if (newProps.userDetails != this.state.userDetails) {
      this.setState({
        ...this.state,
        userDetails: newProps.userDetails
      })
    }
  }
  handleSubmit = (e) => {
    e.preventDefault()
    this.props.addEdit(this.state.userDetails)
  }
  handleChange = (e) => {
    e.preventDefault()
    const mailRegex = /^(([^<>()\[\]\.,:\s@\"]+(\.[^<>()\[\]\.,:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,:\s@\"]+\.)+[^<>()[\]\.,:\s@\"]{2,})$/i
    const phoneRegex = /^((\+)?(\d{2}[-])?(\d{10}){1})?(\d{11}){0,1}?$/
    const { name, value } = e.target
    let errors = this.state.errors
    switch(name) {
      case 'firstName':
        errors.firstName = (value.length > 0) ? '' : 'Please enter the first name'
        break
      case 'lastName':
        errors.lastName = (value.length > 0) ? '' : 'Please enter the last name'
        break
      case 'age':
        errors.age = (value > 13) ? '' : 'You must be above 13 years old'
        break
      case 'mail':
        errors.mail = (value.length > 0 && value.match(mailRegex)) ? '' : 'Please enter a valid email'
        break
      case 'mobile':
        errors.mobile = (value.length > 0 && value.match(phoneRegex)) ? '' : 'Please enter a valid phone number'
        break
      default:
        // continue
    }
    this.setState({
      errors,
      userDetails: {
        ...this.state.userDetails,
        [name]: value
      }
    })
  }
  render() {
    const { firstName, lastName, age, mail, mobile } = this.props.userDetails
    return(
      <div className="user-basic-details-container">
        <div className="profile-picture"></div>
        <div id="full-name">
          <form key={ firstName } onSubmit={ this.handleSubmit }>
            <input className="text" id="name" name="firstName" defaultValue={ firstName } onChange={ this.handleChange }/>
          </form>
          <form key={ lastName } onSubmit={ this.handleSubmit }>
            <input className="text" id="name" name="lastName" defaultValue={ lastName } onChange={ this.handleChange }/>
          </form>
        </div>
        <form key={ age } onSubmit={ this.handleSubmit }>
          <input className="text" id="age" name="age" defaultValue={ age } onChange={ this.handleChange }/>
        </form>
        <form key={ mail } onSubmit={ this.handleSubmit }>
          <input className="text" id="contact" name="mail" defaultValue={ mail } onChange={ this.handleChange }/>
        </form>
        <form key={ mobile } onSubmit={ this.handleSubmit }>
          <input className="text" id="contact" name="mobile" defaultValue={ mobile } onChange={ this.handleChange }/>
        </form>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: state.userDetails
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addEdit: (userDetails) => {
      console.log(userDetails)
      dispatch({
        type: 'ADD_EDIT',
        payload: userDetails
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Details)
