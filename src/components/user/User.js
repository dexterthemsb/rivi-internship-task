import React from 'react'
import { connect } from 'react-redux'

import './index.css'

const User = (props) => {
  const { firstName, lastName, age, mail, mobile } = props.userDetails
  const dataPresent = firstName.length > 0 ? true : false
  return(
    <div className="user-container">
      <label for="favicon-toggle" id="user-favicon"></label>
      <input type="checkbox" id="favicon-toggle"/>
      <div id="details-drop">
        { dataPresent == true && (<p>{ firstName } { lastName }<br/>{ mail }<br/>{ mobile } </p>) }
      </div>
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    userDetails: state.userDetails
  }
}

export default connect(mapStateToProps)(User)
