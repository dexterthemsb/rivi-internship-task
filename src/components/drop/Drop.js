import React from 'react'
import './index.css'
import { TextField, InputBase, Button, Menu } from '@material-ui/core'
import SearchIcon from '@material-ui/icons/Search'

const Drop = () => {
  const [anchorEl, setAnchorEl] = React.useState(null)
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget)
  }
  const handleClose = () => {
    setAnchorEl(null)
  }
  return(
    <div className="drop-container" onClick={ handleClick }>
      {/* <Button id="toggle" aria-controls="simple-menu" aria-haspopup="true" onClick={ handleClick }></Button>
      <Menu id="simple-menu" anchorEl={ anchorEl } keepMounted open={ Boolean(anchorEl) } onClose={ handleClose }>
        <p>Manmohan Singh</p>
      </Menu> */}
      <div className="search-bar">
        <ul className="search-bar-ul">
          <li className="icon"><SearchIcon color="#808080" fontSize="small"/></li>
          <InputBase placeholder="Search" id="search-naked"/>
        </ul>
      </div>
    </div>
  )
}

export default Drop
