import React, { Component } from 'react'
import { connect } from 'react-redux'

import { TextField, Button } from '@material-ui/core'

import './index.css'

const isValid = (userDetails) => {
  let valid = true
  Object.values(userDetails).forEach(userDetail => {
    userDetail.length == 0 && (valid = false)
  })
  return valid
}

class Form extends Component {
  constructor(props) {
    super(props)
    this.state = {
      userDetails: props.userDetails,
      errors: {
        lastName: '',
        firstName: '',
        age: '',
        mail: '',
        mobile: ''
      },
      submitError: ''
    }
  }
  componentWillReceiveProps(newProps) {
    if (newProps.userDetails != this.state.userDetails) {
      this.setState({
        ...this.state,
        userDetails: newProps.userDetails
      })
    }
  }
  handleSubmit = (e) => {
    e.preventDefault()
    if (isValid(this.state.userDetails)) {
      console.log('Action dispatched')
      this.props.addEdit(this.state.userDetails)
    } else {
      console.log('Error')
      this.setState({
        submitError: 'Please check the mandatory fields before submitting the form'
      })
    }
  }
  handleInput = (e) => {
    e.preventDefault()
    const mailRegex = /^(([^<>()\[\]\.,:\s@\"]+(\.[^<>()\[\]\.,:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,:\s@\"]+\.)+[^<>()[\]\.,:\s@\"]{2,})$/i
    const phoneRegex = /^((\+)?(\d{2}[-])?(\d{10}){1})?(\d{11}){0,1}?$/
    const { name, value } = e.target
    let errors = this.state.errors
    switch(name) {
      case 'firstName':
        errors.firstName = (value.length > 0) ? '' : 'Please enter the first name'
        break
      case 'lastName':
        errors.lastName = (value.length > 0) ? '' : 'Please enter the last name'
        break
      case 'age':
        errors.age = (value > 13) ? '' : 'You must be above 13 years old'
        break
      case 'mail':
        errors.mail = (value.length > 0 && value.match(mailRegex)) ? '' : 'Please enter a valid email'
        break
      case 'mobile':
        errors.mobile = (value.length > 0 && value.match(phoneRegex)) ? '' : 'Please enter a valid phone number'
        break
      default:
        // continue
    }
    this.setState({
      errors,
      userDetails: {
        ...this.state.userDetails,
        [name]: value
      }
    })
  }
  render() {
    const { firstName, lastName, age, mail, mobile} = this.props.userDetails
    const { errors } = this.state
    return(
      <div className="main-form-container">
        <p className="heading">ENTER YOUR DETAILS</p>
        <form key={ firstName } id="first-name-form" onSubmit={ this.handleSubmit }>
          <TextField id="form-fields" type="text" name="firstName" defaultValue={ firstName } onChange={ this.handleInput } label="FIRST NAME" />
          { errors.firstName.length > 0 && (<p id="component-error-text">{ errors.firstName }</p>) }
        </form>
        <form key={ lastName } id="first-name-form" onSubmit={ this.handleSubmit }>
          <TextField id="form-fields" type="text" name="lastName" defaultValue={ lastName } onChange={ this.handleInput } label="LAST NAME"/>
          { errors.lastName.length > 0 && (<p id="component-error-text">{ errors.lastName }</p>) }
        </form>
        <form key={ age } id="first-name-form" onSubmit={ this.handleSubmit }>
          <TextField id="form-fields" type="number" name="age" defaultValue={ age } onChange={ this.handleInput } label="AGE"/>
          { errors.age.length > 0 && (<p id="component-error-text">{ errors.age }</p>) }
        </form>
        <form key={ mail } id="first-name-form" onSubmit={ this.handleSubmit }>
          <TextField id="form-fields" type="text" name="mail" defaultValue={ mail } onChange={ this.handleInput } label="MAIL"/>
          { errors.firstName.length > 0 && (<p id="component-error-text">{ errors.mail }</p>) }
        </form>
        <form key={ mobile } id="first-name-form" onSubmit={ this.handleSubmit }>
          <TextField id="form-fields" type="number" name="mobile" defaultValue={ mobile } onChange={ this.handleInput } label="MOBILE"/>
          { errors.mobile.length > 0 && (<p id="component-error-text">{ errors.mobile }</p>) }
        </form>
        <Button type="submit" onClick={ this.handleSubmit }>Submit</Button>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    userDetails: state.userDetails
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addEdit: (userDetails) => {
      console.log(userDetails)
      dispatch({
        type: 'ADD_EDIT',
        payload: userDetails
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Form)
