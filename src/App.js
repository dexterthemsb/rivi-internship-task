import React, { Component } from 'react'
import { Grid } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu';

import Form from './components/form/Form'
import Details from './components/details/Details'
import Drop from './components/drop/Drop'
import User from './components/user/User'

import './App.css'

class App extends Component {
  render() {
    return (
      <div className="app-container">
        <label for="menu-toggle" id="tog-label">
          <MenuIcon id="tog-icon" fontSize="medium"/>
        </label>
        <input type="checkbox" id="menu-toggle"/>
        <div id="nav-container">
          <p id="message">NAVIGATION AREA</p>
        </div>
        <Drop className="drop" id="drop-component" />
        <User />
        <Grid container>
          <Grid item md={ 8 }>
            <Form id="from"/>
          </Grid>
          <Grid item md={ 4 }>
            <Details id="details"/>
          </Grid>
        </Grid>
      </div>
    )
  }
}

export default App
