const intiState = {
  userDetails: {
    firstName: 'Manmohan',
    lastName: 'Singh',
    age: '22',
    mail: 'dexterthemsb@gmail.com',
    mobile: '7338763973'
  }
}

const rootReducer = (state = intiState, action) => {
  if (action.type === 'ADD_EDIT') {
    const payload = action.payload
    return {
      ...state,
      userDetails: payload
    }
  }
  return state
}

export default rootReducer
